<?php
/**
 * Custom login setup
 *
 * @package wpb
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

add_shortcode( 'faq', 'faqs' );

function faqs( $atts ) {
	global $post;
	$a = shortcode_atts( array( 'faq-cat' => '0' ), $atts );

	$output = '';

	// The Query
	$faq_cat = esc_attr( $a['faq-cat']);

    if ($faq_cat) {
		$args = array (
			'post_type' => 'faq',
		    'posts_per_page' => -1,
		    'tax_query' => array(
				array(
					'taxonomy' => 'faq_cats', // taxonomy name
					'field' => 'term_id',    // term_id, slug or name
					'terms' => $faq_cat,     // term id, term slug or term name
			 	)
		 	)
	 	);
	} else {
		$args = array (
			'post_type' => 'faq',
			'posts_per_page' => -1,
	 	);
	}

	$the_faq_query = new WP_Query( $args );

	if ( $the_faq_query->have_posts() ) {
		$post_id = get_the_ID();
		$output .= '<div class="accordion accordion-flush" id="accordion-'.$post_id.'">';

		while ( $the_faq_query->have_posts() ) {
			$the_faq_query->the_post();
			$faq_id = get_the_ID();
			$faq_title = get_the_title();
			$faq_content = get_the_content();

			$output .= '<div class="accordion-item faq">
				  <div class="accordion-header p-0" id="heading-'.$faq_id.'">
					  <button class="accordion-button collapsed " type="button" data-bs-toggle="collapse" data-bs-target="#collapse'.$faq_id.'" aria-expanded="false" aria-controls="collapse'.$faq_id.'">
						<h5 class="faq-title p-3 m-0">'.$faq_title.'</h5>
					  </button>
				  </div>
				  <div id="collapse'.$faq_id.'" class="collapse" aria-labelledby="heading'.$faq_id.'" data-bs-parent="#accordion-'.$post_id.'">
					<div class="accordion-body">'.$faq_content.'</div>
				  </div>
				</div>';
			}
			$output .= '</div>';
		}

	wp_reset_postdata();
	return $output;
}
