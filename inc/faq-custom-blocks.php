<?php
/**
 * Custom login setup
 *
 * @package wpb
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( function_exists( 'register_block_style' ) ) {
	acf_register_block_type(array(
		'name'              => 'faq',
		'title'             => __('FAQs'),
		'description'       => __('A block to show FAQs.'),
		'render_template'   => plugin_dir_path( __FILE__ ) . 'partials/block-faqs.php',
		//'enqueue_style'     => plugin_dir_path( __FILE__ )  . 'css/style.css',
		'category'          => 'widgets',
		'icon'              => 'editor-help',
		'keywords'          => array( 'faq')
	));
}
