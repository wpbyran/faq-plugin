<?php
/*
* Enqueue scripts.js if file scripts.js exists
*/
function load_scripts() {

	wp_enqueue_style( 'faq-css', plugin_dir_url( __FILE__ )  . 'css/style.css');
	wp_enqueue_style( 'bootstrap-css', plugin_dir_url( __FILE__ )  . 'bootstrap/css/bootstrap.css');

	wp_enqueue_script('bootstrap-js', plugin_dir_url( __FILE__ )  . 'bootstrap/js/bootstrap.bundle.min.js');
}

add_action( 'wp_enqueue_scripts', 'load_scripts' );
