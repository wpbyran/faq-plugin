<?php
/**
 * Plugin Name:     FAQ plugin
 * Plugin URI:
 * Description:     Custom post types for FAQ using Bootstrap collapse
 * Author:          WP_Byrån
 * Author URI:      https://wpbyran.se
 * Text Domain:     wpb_faq
 * Domain Path:     /languages
 * Version:         1.0.2
 *
 */

function my_plugin_init() {
	load_plugin_textdomain( 'wpb_faq', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'my_plugin_init' );

require_once('inc/scripts.php');
require_once 'acf.php';
require_once 'inc/faq-custom-blocks.php';
require_once 'inc/faq-shortcode.php';

// Register custom post types
function wpb_faq__custom_post_types() {

	register_post_type('faq',
	array(
		'labels'      => array(
			'name'          => __('FAQ', 'wpb_faq'),
			'singular_name' => __('FAQ', 'wpb_faq'),
			'name_admin_bar' => __('FAQ', 'wpb_faq'),
		),
		'description' => __('Frequently Asked Questions', 'wpb_faq'),
		'public'      => true,
		'has_archive' => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'menu_icon'           => 'dashicons-format-chat',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'faq' ),
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'menu_position'      => 21,
		'supports'           => array( 'title', 'editor' ),
		'show_in_rest'       => true,
		'taxonomies'            => array( 'faq_cats'),
	)
);

}
add_action('init', 'wpb_faq__custom_post_types');

// Register Custom Taxonomy
function wpb_faq__custom_taxonomy_faq_cats() {

	$labels = array(
		'name'                       => _x( 'FAQ categories', 'Taxonomy General Name', 'wpb_faq' ),
		'singular_name'              => _x( 'FAQ category', 'Taxonomy Singular Name', 'wpb_faq' ),
		'menu_name'                  => __( 'FAQ categories', 'wpb_faq' ),
		'all_items'                  => __( 'All FAQ categories', 'wpb_faq' ),
		'parent_item'                => __( 'Parent FAQ category', 'wpb_faq' ),
		'parent_item_colon'          => __( 'Parent FAQ category:', 'wpb_faq' ),
		'new_item_name'              => __( 'New FAQ category', 'wpb_faq' ),
		'add_new_item'               => __( 'Add New FAQ category', 'wpb_faq' ),
		'edit_item'                  => __( 'Edit FAQ category', 'wpb_faq' ),
		'update_item'                => __( 'Update FAQ category', 'wpb_faq' ),
		'view_item'                  => __( 'View FAQ category', 'wpb_faq' ),
		'separate_items_with_commas' => __( 'Separate FAQ categories with commas', 'wpb_faq' ),
		'add_or_remove_items'        => __( 'Add or remove FAQ categories', 'wpb_faq' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpb_faq' ),
		'popular_items'              => __( 'Popular FAQ categories', 'wpb_faq' ),
		'search_items'               => __( 'Search FAQ categories', 'wpb_faq' ),
		'not_found'                  => __( 'Not Found', 'wpb_faq' ),
		'no_terms'                   => __( 'No FAQ categories', 'wpb_faq' ),
		'items_list'                 => __( 'Items list', 'wpb_faq' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wpb_faq' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'faq_cats', array( 'faq' ), $args );

}
add_action( 'init', 'wpb_faq__custom_taxonomy_faq_cats', 0 );

function faq_css() {
	wp_register_style( 'faq-css', plugins_url('/css/style.css',  __FILE__ ) );
	wp_enqueue_style( 'faq-css');
}
